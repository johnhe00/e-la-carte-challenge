#ifndef BLOCK_H
#define BLOCK_H

#include <iostream>

class Block {

private:
	int startx;
	int endx;
	int starty;
	int endy;
	bool changed;

public:
	void setBlock(int sx, int ex, int sy, int ey) {
		endx = ex;
		startx = sx;
		starty = sy;
		endy = ey;
		changed = true;
	}
	
	int getArea() {
		return (endx - startx + 1) * (endy - starty + 1);
	}

	void addHeight() {
		endy++;
		changed = true;
	}

	// Setters
	void setStartx(int x) {
		startx = x;
		changed =true;
	}

	void setEndx(int x) {
		endx = x;
		changed = true;
	}

	void setStarty(int y) {
		starty = y;
		changed = true;
	}

	void setEndy(int y) {
		endy = y;
		changed = true;
	}

	void setChanged(bool c) {
		changed = c;
	}

	// Getters
	int getStartx() {
		return startx;
	}

	int getEndx() {
		return endx;
	}

	int getStarty() {
		return starty;
	}

	int getEndy() {
		return endy;
	}

	bool isChanged() {
		return changed;
	}
};

#endif
