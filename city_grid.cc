#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include "block.h"

#define PRESTORANT 'P'

using namespace std;

// Remove blocks that have no potential growth, sets largest block
void cleanList(list<Block> &, Block &);

// Read in the file to a grid of only Prestorants
vector<vector<int> > readFile(const char *);

// Given a grid of Prestorants, return the largest contiguous block in the grid
Block findLargestBlock(vector<vector<int> > &);

int main() {
	const char * filename = "input_city_grid.txt";
	vector<vector<int> > restaurant = readFile(filename);
	Block largestBlock = findLargestBlock(restaurant);
	
	cout << "The largest area is: " << largestBlock.getArea() << endl;
	cout << "Width found from index " << largestBlock.getStartx() << " to " << largestBlock.getEndx() << endl;
	cout << "Height found from index " << largestBlock.getStarty() << " to " << largestBlock.getEndy() << endl;

	return 0;
}

Block findLargestBlock(vector<vector<int> >&restaurant) {
	Block largestBlock;
	largestBlock.setBlock(1,1,1,-1);
	list<Block> growingBlocks;

	for(int i = 0; i < restaurant.size(); i++) {
		for(int j = 0; j < restaurant[i].size(); j++) {
			// Get a contiguous line of Prestorants
			int start = restaurant[i][j];
			while(j + 1 < restaurant[i].size() && (restaurant[i][j + 1] - restaurant[i][j]) == 1) {
				j++;
			}
			int end = restaurant[i][j];

			// See if it connects with a block of Prestorants from the previous line
			if(!growingBlocks.empty()) {
				list<Block>::iterator it;
				list<Block> newBlocks;
				bool addEntry = true;
				for(it = growingBlocks.begin(); it != growingBlocks.end(); it++) {
					// Outside of the position of a block from the previous line
					if((*it).getStartx() > end || (*it).getEndx() < start) {
						continue;
					}
					// Exactly the position of a block from the previous line
					else if((*it).getStartx() == start && (*it).getEndx() == end) {
						(*it).addHeight();
						addEntry = false;
						break;
					}
					// Intersection in the position of a block from the previous line
					else {
						// Create block within the intersection
						Block b;
						b.setStarty((*it).getStarty());
						b.setEndy(i);
						b.setStartx(start >= (*it).getStartx() ? start : (*it).getStartx());
						b.setEndx(end <= (*it).getEndx() ? end : (*it).getEndx());
						newBlocks.push_back(b);

						if(start >= (*it).getStartx() && end <= (*it).getEndx()) {
							addEntry = false;
							break;
						}
					}
				}
				if(addEntry) {
					Block b;
					b.setBlock(start, end, i, i);
					newBlocks.push_back(b);
				}
				// Add newly created blocks to the list of growing blocks
				growingBlocks.splice(growingBlocks.end(), newBlocks);
			}
			else {
				Block b;
				b.setBlock(start, end, i, i);
				growingBlocks.push_back(b);
			}
		}
		// Remove non-growing blocks
		cleanList(growingBlocks, largestBlock);
	}
	cleanList(growingBlocks, largestBlock);
	return largestBlock;
}

void cleanList(list<Block> &growingBlocks, Block &largestBlock) {
	list<Block>::iterator it;
	for(it = growingBlocks.begin(); it != growingBlocks.end();) {
		if(!(*it).isChanged()) {
			if(largestBlock.getArea() < (*it).getArea()) {
				largestBlock = *it;
			}
			it = growingBlocks.erase(it);
		}
		else {
			(*it).setChanged(false);
			it++;
		}
	}
}

vector<vector<int> > readFile(const char * filename) {
	vector<vector<int> > restaurant;
	ifstream fin(filename, ifstream::in);
	string in;
	while(fin.good()) {
		vector<int> r;
		getline(fin, in);
		if(in.empty()) {
			break;
		}
		for(int i = 0; i < in.length(); i+=4) {
			if(in[i] == PRESTORANT) {
				r.push_back(i/4);
			}
		}
		restaurant.push_back(r);
	}
	fin.close();
	return restaurant;
}
